#!/usr/bin/python3
# -*- coding: utf-8
# vim: set expandtab shiftwidth=4:
# -*- Mode: python; coding: utf-8; indent-tabs-mode: nil -*- */
#
# Copyright © 2019 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the 'Software'),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#
#
# Extracts x/y coordinates and prints them as one event per line with the
# current location (i.e. where one coordinate is missing, the last known one
# is used).
#
# Input is a libinput record yaml file

import argparse
import yaml
import libevdev


def tv2us(sec, usec):
    return 1000000 * sec + usec


class InputEvent:
    def __init__(self, data):
        self.sec = data[0]
        self.usec = data[1]
        self.evtype = data[2]
        self.evcode = data[3]
        self.value = data[4]


def main(args):
    yml = yaml.safe_load(open(args.path[0]))
    device = yml['devices'][0]
    absinfo = device['evdev']['absinfo']

    xres = 1.0
    yres = 1.0
    if args.use_mm:
        xres *= absinfo[libevdev.EV_ABS.ABS_X.value][4]
        yres *= absinfo[libevdev.EV_ABS.ABS_Y.value][4]

    x, y = 0, 0
    toffset = None
    for event in device['events']:
        for evdev in event['evdev']:
            e = InputEvent(evdev)
            evbit = libevdev.evbit(e.evtype, e.evcode)
            if evbit == libevdev.EV_REL.REL_X:
                x = e.value
            elif evbit == libevdev.EV_REL.REL_Y:
                y = e.value
            elif evbit == libevdev.EV_ABS.ABS_X:
                x = e.value
            elif evbit == libevdev.EV_ABS.ABS_Y:
                y = e.value
            elif evbit == libevdev.EV_SYN.SYN_REPORT:
                if toffset is None:
                    toffset = tv2us(e.sec, e.usec)
                time = tv2us(e.sec, e.usec) - toffset
                print(f'{time}\t{x}\t{y}')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Extract x/y coordinate data from an event stream")
    parser.add_argument("--use-mm", action='store_true', help="Use mm instead of device deltas")
    parser.add_argument("path", metavar="recording", nargs=1, help="Path to libinput-record YAML file")
    args = parser.parse_args()

    main(args)
