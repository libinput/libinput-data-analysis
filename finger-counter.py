#!/usr/bin/python3
# -*- coding: utf-8
# vim: set expandtab shiftwidth=4:
# -*- Mode: python; coding: utf-8; indent-tabs-mode: nil -*- */
#
# Copyright © 2019 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the 'Software'),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#
#
# Shows the number of fingers on the device at any time
#
# Input is a libinput record yaml file

import argparse
import sys
import yaml
import libevdev


class InputEvent:
    def __init__(self, data):
        self.sec = data[0]
        self.usec = data[1]
        self.evtype = data[2]
        self.evcode = data[3]
        self.value = data[4]


def main(argv):
    parser = argparse.ArgumentParser(description="Count number of fingers on the device")
    parser.add_argument("path", metavar="recording",
                        nargs=1, help="Path to libinput-record YAML file")
    args = parser.parse_args()

    yml = yaml.safe_load(open(args.path[0]))
    device = yml['devices'][0]

    last_time = None

    changed = True
    finger_bits = 0
    fmap = {
        libevdev.EV_KEY.BTN_TOOL_FINGER:    (1 << 0),
        libevdev.EV_KEY.BTN_TOOL_DOUBLETAP: (1 << 1),
        libevdev.EV_KEY.BTN_TOOL_TRIPLETAP: (1 << 2),
        libevdev.EV_KEY.BTN_TOOL_QUADTAP:   (1 << 3),
        libevdev.EV_KEY.BTN_TOOL_QUINTTAP:  (1 << 4),
    }

    for event in device['events']:
        for evdev in event['evdev']:
            e = InputEvent(evdev)
            evbit = libevdev.evbit(e.evtype, e.evcode)

            try:
                if e.value:
                    finger_bits |= fmap[evbit]
                else:
                    finger_bits &= ~fmap[evbit]
                changed = True
            except KeyError:
                pass

            if evbit == libevdev.EV_SYN.SYN_REPORT:
                if last_time is None:
                    last_time = e.sec * 1000000 + e.usec
                    tdelta = 0
                elif changed:  # we only calculate delta between finger changes
                    t = e.sec * 1000000 + e.usec
                    tdelta = (t - last_time) // 1000  # ms
                    last_time = t

                if changed:
                    print("{:2d}.{:06d} {:+6d}ms: ".format(e.sec, e.usec, tdelta), end="")

                    if finger_bits == 0:
                        print("---------------")
                    else:
                        mask = finger_bits
                        idx = 1
                        while mask:
                            print("-{}- ".format(idx), end="")
                            mask >>= 1
                            idx += 1
                        print("")

                    changed = False


if __name__ == '__main__':
    main(sys.argv)
